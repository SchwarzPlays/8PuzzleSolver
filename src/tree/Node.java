package tree;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Brian Anello
 */
public class Node {
    private String moves;
    private Node parent;
    private ArrayList<Node> child;
    private ArrayList<Integer> currPos;
    private static ArrayList<Integer> answer;
    
    private int depth;
    /**
     * Makes a new Node with the move that was done to create it.
     * @param move Move in a direction that was done to make this child
     */
    Node(char move, ArrayList<Integer> al, String prev) {
        child = new ArrayList<Node>(1);
        moves = prev;
        moves += move;
        currPos = al;
        depth = moves.length();
    }
    /**
     * Makes a new node that is the root node of the tree
     * @param al The list of the current state
     * @param ans The goal state
     */
    Node(ArrayList<Integer> al, ArrayList<Integer> ans) {
        depth = 0;
        moves = "";
        currPos = al;
        answer = ans;
        child = new ArrayList<Node>(1);
    }
    /**
     * Makes the children of the current Node and puts them into the ArrayList child.
     * Takes into account what the previous move was.
     */
    public void makeBabies() {
        
        if(currPos.indexOf(0) == 0) { //Can move R, D
            //System.out.println("0th");
            if(moves != "") {
                int x = moves.length();
                char c = moves.charAt(--x);
                if(c == 'R') {                    
                    child.add(new Node('D', swap(currPos, 0, 3), getMoves()));
                } else if(c == 'D') {
                    child.add(new Node('R', swap(currPos, 0, 1), getMoves()));                    
                } else {
                    // Should never enter this else.
                }
            } else {
                child.add(new Node('D', swap(currPos, 0, 3), getMoves()));
                child.add(new Node('R', swap(currPos, 0, 1), getMoves()));
            }            
        } else if(currPos.indexOf(0) == 1) { //Can move L, D, R
            //System.out.println("1st");
            if(moves != "") {
                int x = moves.length();
                char c = moves.charAt(--x);
                if(c == 'L') {                    
                    child.add(new Node('D', swap(currPos, 1, 4), getMoves()));
                    child.add(new Node('R', swap(currPos, 1, 2), getMoves()));  
                } else if(c == 'U') {
                    child.add(new Node('L', swap(currPos, 1, 0), getMoves())); 
                    child.add(new Node('R', swap(currPos, 1, 2), getMoves()));                    
                } else if(c == 'R') {
                    child.add(new Node('L', swap(currPos, 1, 0), getMoves())); 
                    child.add(new Node('D', swap(currPos, 1, 4), getMoves()));
                } else {
                    // Should never enter this else.
                }
            } else {
                child.add(new Node('L', swap(currPos, 1, 0), getMoves())); 
                child.add(new Node('D', swap(currPos, 1, 4), getMoves()));
                child.add(new Node('R', swap(currPos, 1, 2), getMoves()));
            }                     
        } else if(currPos.indexOf(0) == 2) { //Can move L, D
            //System.out.println("2nd");
            if(moves != "") {
                int x = moves.length();
                char c = moves.charAt(--x);
                if(c == 'L') {                    
                    child.add(new Node('D', swap(currPos, 2, 5), getMoves()));
                } else if(c == 'D') {
                    child.add(new Node('L', swap(currPos, 2, 1), getMoves()));                    
                } else {
                    // Should never enter this else.
                }
            } else {
                child.add(new Node('L', swap(currPos, 2, 1), getMoves()));
                child.add(new Node('D', swap(currPos, 2, 5), getMoves()));
            }                 
        } else if(currPos.indexOf(0) == 3) { //Can move U, R, D
            //System.out.println("3rd");
            if(moves != "") {
                int x = moves.length();
                char c = moves.charAt(--x);
                if(c == 'D') {                    
                    child.add(new Node('D', swap(currPos, 3, 6), getMoves()));
                    child.add(new Node('R', swap(currPos, 3, 4), getMoves()));  
                } else if(c == 'U') {
                    child.add(new Node('U', swap(currPos, 3, 0), getMoves())); 
                    child.add(new Node('R', swap(currPos, 3, 4), getMoves()));                    
                } else if(c == 'L') {
                    child.add(new Node('U', swap(currPos, 3, 0), getMoves())); 
                    child.add(new Node('D', swap(currPos, 3, 6), getMoves()));
                } else {
                    // Should never enter this else.
                }
            } else {
                child.add(new Node('U', swap(currPos, 3, 0), getMoves())); 
                child.add(new Node('D', swap(currPos, 3, 6), getMoves()));
                child.add(new Node('R', swap(currPos, 3, 4), getMoves()));
            }            
        } else if(currPos.indexOf(0) == 4) { //Can move U, L, R, D
            //System.out.println("4th");
            if(moves != "") {
                int x = moves.length();
                char c = moves.charAt(--x);
                if(c == 'D') {             
                    child.add(new Node('L', swap(currPos, 4, 3), getMoves()));
                    child.add(new Node('R', swap(currPos, 4, 5), getMoves())); 
                    child.add(new Node('D', swap(currPos, 4, 7), getMoves())); 
                } else if(c == 'R') {
                    child.add(new Node('U', swap(currPos, 4, 1), getMoves())); 
                    child.add(new Node('R', swap(currPos, 4, 5), getMoves()));
                    child.add(new Node('D', swap(currPos, 4, 7), getMoves())); 
                } else if(c == 'L') {
                    child.add(new Node('U', swap(currPos, 4, 1), getMoves())); 
                    child.add(new Node('L', swap(currPos, 4, 3), getMoves()));
                    child.add(new Node('D', swap(currPos, 4, 7), getMoves()));
                } else if(c == 'U') {
                    child.add(new Node('U', swap(currPos, 4, 1), getMoves())); 
                    child.add(new Node('L', swap(currPos, 4, 3), getMoves()));
                    child.add(new Node('R', swap(currPos, 4, 5), getMoves())); 
                } else {
                    // Should never enter this else.
                }
            } else {
                child.add(new Node('U', swap(currPos, 4, 1), getMoves())); 
                child.add(new Node('L', swap(currPos, 4, 3), getMoves()));
                child.add(new Node('D', swap(currPos, 4, 7), getMoves()));
                child.add(new Node('R', swap(currPos, 4, 5), getMoves()));
            }             
        } else if(currPos.indexOf(0) == 5) { //Can move U, L, D
            //System.out.println("5th");
            if(moves != "") {
                int x = moves.length();
                char c = moves.charAt(--x);
                if(c == 'D') {                    
                    child.add(new Node('L', swap(currPos, 5, 4), getMoves()));
                    child.add(new Node('D', swap(currPos, 5, 8), getMoves()));  
                } else if(c == 'L') {
                    child.add(new Node('U', swap(currPos, 5, 2), getMoves())); 
                    child.add(new Node('D', swap(currPos, 5, 8), getMoves()));                    
                } else if(c == 'U') {
                    child.add(new Node('U', swap(currPos, 5, 2), getMoves())); 
                    child.add(new Node('L', swap(currPos, 5, 4), getMoves()));
                } else {
                    // Should never enter this else.
                }
            } else {
                child.add(new Node('U', swap(currPos, 5, 2), getMoves())); 
                child.add(new Node('L', swap(currPos, 5, 4), getMoves()));
                child.add(new Node('D', swap(currPos, 5, 8), getMoves()));
            }            
        } else if(currPos.indexOf(0) == 6) { //Can move U, R
            //System.out.println("6th");
            if(moves != "") {
                int x = moves.length();
                char c = moves.charAt(--x);
                if(c == 'D') {                    
                    child.add(new Node('R', swap(currPos, 6, 7), getMoves()));
                } else if(c == 'L') {
                    child.add(new Node('U', swap(currPos, 6, 3), getMoves()));                    
                } else {
                    // Should never enter this else.
                }
            } else {
                child.add(new Node('U', swap(currPos, 6, 3), getMoves()));
                child.add(new Node('R', swap(currPos, 6, 7), getMoves()));
            }
        } else if(currPos.indexOf(0) == 7) { //Can move L, U, R
            //System.out.println("7th");
            if(moves != "") {
                int x = moves.length();
                char c = moves.charAt(--x);
                if(c == 'D') {                    
                    child.add(new Node('L', swap(currPos, 7, 6), getMoves()));
                    child.add(new Node('R', swap(currPos, 7, 8), getMoves()));  
                } else if(c == 'R') {
                    child.add(new Node('U', swap(currPos, 7, 4), getMoves())); 
                    child.add(new Node('R', swap(currPos, 7, 8), getMoves()));                    
                } else if(c == 'L') {
                    child.add(new Node('U', swap(currPos, 7, 4), getMoves())); 
                    child.add(new Node('L', swap(currPos, 7, 6), getMoves()));
                } else {
                    // Should never enter this else.
                }
            } else {
                child.add(new Node('U', swap(currPos, 7, 4), getMoves())); 
                child.add(new Node('L', swap(currPos, 7, 6), getMoves()));
                child.add(new Node('R', swap(currPos, 7, 8), getMoves()));
            }
        } else if(currPos.indexOf(0) == 8) { //Can move L, U
            //System.out.println("8th");
            if(moves != "") {
                int x = moves.length();
                char c = moves.charAt(--x);
                if(c == 'R') {                    
                    child.add(new Node('U', swap(currPos, 8, 5), getMoves()));
                } else if(c == 'D') {
                    child.add(new Node('L', swap(currPos, 8, 7), getMoves()));                    
                } else {
                    // Should never enter this else.
                }
            } else {
                child.add(new Node('L', swap(currPos, 8, 7), getMoves()));
                child.add(new Node('U', swap(currPos, 8, 5), getMoves()));
            }
        } else {
            // Should never enter this else.
        }
    }
    /**
     * Swaps the places of two integers
     * @param al List for swapping
     * @param i First spot to be swapped
     * @param j Second spot to be swapped
     * @return The ArrayList after the swap
     */
    public ArrayList<Integer> swap(ArrayList<Integer> al, int i, int j) {
        ArrayList<Integer> temp = new ArrayList<Integer>(9);
        for(int x = 0; x < 9; ++x) {
            temp.add(x);
        }
        Collections.copy(temp, al);
        Collections.swap(temp, i, j);
        return temp;
    }
    /**
     * Returns the moves done so far
     * @return moves
     */
    public String getMoves() {
        return moves;
    }
    /**
     * The toString method that shows the contents of the Node.
     * @return the String of this Node
     */
    public String toString() {
        return "<-Node->\n" + "Current state: " + getCurrPos().toString() + "\n" + "Depth: " + depth + "\n" + "Moves: " + 
                moves + "\n";        
    }
    /**
     * Gets the current position of all the numbers
     * @return The ArrayList of the current position of the numbers
     */
    public ArrayList<Integer> getCurrPos() {
        return currPos;
    }
    /**
     * Finds out if this is the correct state
     * @return True, correct state; False, not correct
     */
    public boolean findMoves() {
        boolean ch = true;
        for(int i = 0; i < 9; ++i) {
            if(currPos.get(i) != answer.get(i)) {
                return false;
            }
        }
        return ch;
    }
    /**
     * Returns the list of the children
     * @return children in an ArrayList
     */
    public ArrayList<Node> getBabies() {
        return child;
    }
    /**
     * Get the depth of the node
     * @return depth
     */
    public int getDepth() {
        return depth;
    }
    /**
     * Checks the node and sees if it is solvable from its current state
     * @return True, if solvable; False, not solvable
     */
    public boolean isSolvable() {
        int i = 0;
        for(int x = 0; x < 9; ++x) {
            int y = x+1;
            if(currPos.get(x) != 0) {
                while(y < 9) {
                    if(currPos.get(x) > currPos.get(y) && currPos.get(y) != 0) ++i;
                    ++y;
                }
            }
        }
        if(i % 2 == 0) return true;
        return false;
    }
}